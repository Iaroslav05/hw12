const { Op, fn, col } = require("sequelize");
const { Videos } = require("./entities/videos");
const { sequelize } = require("./sequelize");
const commandLineArgs = require("command-line-args");

const optionDefinitions = [{ name: "search", alias: "s", type: String }];
const options = commandLineArgs(optionDefinitions);

const searchQuery = options.search;

const getVideosByTitle = async () => {
  try {
     if (!searchQuery) {
       throw new Error("Search term is required.");
     }
    const videos = await Videos.findAll({
      where: {
        [Op.and]: sequelize.where(fn("LOWER", col("title")), {
          [Op.like]: `%${searchQuery.toLowerCase()}%`,
        }),
      },
    });
    console.log(videos);
    videos.forEach((video) => console.log(video.toJSON()));
  } catch (error) {
    console.error(error);
  } finally {
    await sequelize.close();
  }
};
getVideosByTitle();
