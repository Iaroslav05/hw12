const { Videos } = require("./entities/videos");
const { sequelize } = require("./sequelize");
const {videosList} = require("./fake-data");


const createPost = async () => {
  try {
	for (const video of videosList) {
		const createdPost = await Videos.create({ ...video });
    console.log(createdPost.toJSON());
	}
  } catch (error) {
    console.error(error);
  } finally {
    await sequelize.close();
  }
};
createPost();
