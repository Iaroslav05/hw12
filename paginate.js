const { Videos } = require("./entities/videos");
const { sequelize } = require("./sequelize");

const commandLineArgs = require("command-line-args");

const optionDefinitions = [
  { name: "page", alias: "p", type: Number },
  { name: "size", alias: "s", type: Number },
];
const options = commandLineArgs(optionDefinitions);

const getPagination = async () => {
	try {
		const pagination = await Videos.findAll({
			limit: options.size,
			offset: (options.page - 1) * options.size,
		})
		console.log(pagination);
	} catch (error) {
		console.error(error);
	} finally {
		await sequelize.close();
	}
}
getPagination();