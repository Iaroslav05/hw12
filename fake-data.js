module.exports.videosList = [
  {
    title: "The Shawshank Redemption",
    views: 50,
    category: "Triller",
  },
  {
    title: "The Matrix",
    views: 25,
    category: "Fantastic",
  },
  {
    title: "Lord of the Rings",
    views: 35,
    category: "Fantasy",
  },
  {
    title: "Spiderman",
    views: 34,
    category: "Adventure",
  },
  {
    title: "The Green Mile",
    views: 36,
    category: "Drama",
  },
  {
    title: "Forrest Gump",
    views: 46,
    category: "Сomedy",
  },
  {
    title: "The Terminator",
    views: 56,
    category: "Fantastic",
  },
  {
    title: "Oppenheimer",
    views: 86,
    category: "Biography",
  },
  {
    title: "Schindlers List",
    views: 8,
    category: "History",
  },
];